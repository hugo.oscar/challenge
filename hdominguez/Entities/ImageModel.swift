//
//  ImageModel.swift
//  hdominguez
//
//  Created by Hugo Oscar Dominguez Navarrete on 1/15/19.
//  Copyright © 2019 Hugo Oscar Dominguez Navarrete. All rights reserved.
//

import Foundation
import ObjectMapper

struct ImageModel {
  var id = ""
  var title = ""
  var link = ""
  var cover = ""
}

extension ImageModel: Mappable {

  init?(map: Map) {
  }

  mutating func mapping(map: Map) {
    id <- map["id"]
    title <- map["title"]
    link <- map["link"]
    cover <- map["cover"]
  }
}
