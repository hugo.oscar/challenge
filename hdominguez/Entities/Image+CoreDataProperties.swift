//
//  Image+CoreDataProperties.swift
//  hdominguez
//
//  Created by Hugo Oscar Dominguez Navarrete on 1/15/19.
//  Copyright © 2019 Hugo Oscar Dominguez Navarrete. All rights reserved.
//

import Foundation
import CoreData

extension Image {
  @nonobjc public class func fetchRequest() -> NSFetchRequest<Image> {
    return NSFetchRequest<Image> (entityName: "Image")
  }

  @NSManaged public var link: String?
  @NSManaged public var title: String?
  @NSManaged public var id: String?
  @NSManaged public var cover: String?

}
