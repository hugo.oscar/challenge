//
//  ImageDetailView.swift
//  hdominguez
//
//  Created by Hugo Oscar Dominguez Navarrete on 1/18/19.
//  Copyright © 2019 Hugo Oscar Dominguez Navarrete. All rights reserved.
//

import UIKit
import SDWebImage
import Motion

class ImageDetailView: UIViewController {
	@IBOutlet weak var detailImageView: UIImageView!
	@IBOutlet weak var titleLabel: UILabel!
	var presenter: ImageDetailPresenterProtocol?

	override func viewDidLoad() {
		super.viewDidLoad()
		presenter?.viewDidLoad()
	}
}

extension ImageDetailView: ImageDetailViewProtocol {
	func showImageDetail(forImage image: ImageModel) {
		self.titleLabel.text = image.title
		detailImageView?.sd_setImage(
			with: URL(string: "https://i.imgur.com/\(image.cover).jpg"),
			placeholderImage: UIImage(named: "placeholder"),
			options: SDWebImageOptions.highPriority,
			completed: { (_, _, _, _) in
		})
		isMotionEnabled = true
		detailImageView.motionIdentifier = image.cover
		titleLabel.motionIdentifier = image.title
		detailImageView.transition(.translate(x: -100))
	}
}

fileprivate extension ImageDetailView {
	@objc
	@IBAction func handleCloseButton(button: UIButton) {
		self.dismiss(animated: true) {
		}
	}
}
