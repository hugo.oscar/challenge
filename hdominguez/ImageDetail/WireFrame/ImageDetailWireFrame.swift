//
//  ImageDetailWireFrame.swift
//  hdominguez
//
//  Created by Hugo Oscar Dominguez Navarrete on 1/18/19.
//  Copyright © 2019 Hugo Oscar Dominguez Navarrete. All rights reserved.
//

import UIKit

class ImageDetailWireFrame: ImageDetailWireFrameProtocol {
	class func createImageDetailModule(forImage image: ImageModel) -> UIViewController {
		let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ImageDetailController")
		if let view = viewController as? ImageDetailView {
			let presenter: ImageDetailPresenterProtocol = ImageDetailPresenter()
			let wireFrame: ImageDetailWireFrameProtocol = ImageDetailWireFrame()
			view.presenter = presenter
			presenter.view = view
			presenter.image = image
			presenter.wireFrame = wireFrame
			return viewController
		}
		return UIViewController()
	}
	static var mainStoryboard: UIStoryboard {
		return UIStoryboard(name: "Main", bundle: Bundle.main)
	}
}
