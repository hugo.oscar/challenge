//
//  ImageDetailProtocols.swift
//  hdominguez
//
//  Created by Hugo Oscar Dominguez Navarrete on 1/18/19.
//  Copyright © 2019 Hugo Oscar Dominguez Navarrete. All rights reserved.
//

import UIKit

protocol ImageDetailViewProtocol: class {
	var presenter: ImageDetailPresenterProtocol? { get set }
	func showImageDetail(forImage image: ImageModel)
}

protocol ImageDetailWireFrameProtocol: class {
	static func createImageDetailModule(forImage image: ImageModel) -> UIViewController
}

protocol ImageDetailPresenterProtocol: class {
	var view: ImageDetailViewProtocol? { get set }
	var wireFrame: ImageDetailWireFrameProtocol? { get set }
	var image: ImageModel? { get set }

	func viewDidLoad()
}
