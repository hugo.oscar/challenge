//
//  ImageDetailPresenter.swift
//  hdominguez
//
//  Created by Hugo Oscar Dominguez Navarrete on 1/18/19.
//  Copyright © 2019 Hugo Oscar Dominguez Navarrete. All rights reserved.
//

class ImageDetailPresenter: ImageDetailPresenterProtocol {
	weak var view: ImageDetailViewProtocol?
	var wireFrame: ImageDetailWireFrameProtocol?
	var image: ImageModel?

	func viewDidLoad() {
		view?.showImageDetail(forImage: image ?? ImageModel(id: "", title: "", link: "", cover: ""))
	}
}
