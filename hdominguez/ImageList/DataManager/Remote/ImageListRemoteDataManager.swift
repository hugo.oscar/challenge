//
//  ImageListRemoteDataManager.swift
//  hdominguez
//
//  Created by Hugo Oscar Dominguez Navarrete on 1/15/19.
//  Copyright © 2019 Hugo Oscar Dominguez Navarrete. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

class ImageListRemoteDataManager: ImageListRemoteDataManagerInputProtocol {
  var remoteRequestHandler: ImageListRemoteDataManagerOutputProtocol?
  let headers: HTTPHeaders = [
    "Authorization": "Client-ID 126701cd8332f32"
  ]

  func retrieveImageList() {
    Alamofire
      .request(Endpoints.Images.fetch.url, headers: self.headers)
      .validate()
      .responseArray(keyPath: "data", completionHandler: { (response: DataResponse<[ImageModel]>) in
        switch response.result {
        case .success(let images):
          self.remoteRequestHandler?.onImageRetrieved(images)
        case .failure:
          self.remoteRequestHandler?.onError()
        }
      })
  }

  func retrieveImageListWithString(string: String, iteration: Int) {
    Alamofire
      .request(Endpoints.Images.fetch.searchWithPage(
				page: iteration,
				searchValue: string.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? ""
			),
							 headers: self.headers)
      .validate()
      .responseArray(keyPath: "data", completionHandler: { (response: DataResponse<[ImageModel]>) in
        switch response.result {
        case .success(let images):
          self.remoteRequestHandler?.onImageRetrieved(images)
        case .failure:
          self.remoteRequestHandler?.onError()
        }
      })
  }
}
