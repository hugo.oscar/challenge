//
//  ImageListLocalDataManager.swift
//  hdominguez
//
//  Created by Hugo Oscar Dominguez Navarrete on 1/15/19.
//  Copyright © 2019 Hugo Oscar Dominguez Navarrete. All rights reserved.
//

import CoreData

class ImageListLocalDataManager: ImageListLocalDataManagerInputProtocol {
  func retrieveImageList() throws -> [Image] {
    guard let managedOC = CoreDataStore.managedObjectContext else {
      throw PersistenceError.managedObjectContextNotFound
    }
    let request: NSFetchRequest<Image> = NSFetchRequest(entityName: String(describing: Image.self))
    return try managedOC.fetch(request)
  }
  func saveImage(id: String, title: String, link: String, cover: String) throws {
    guard let managedOC = CoreDataStore.managedObjectContext else {
      throw PersistenceError.managedObjectContextNotFound
    }

    if let newImage = NSEntityDescription.entity(forEntityName: "Image",
                                                in: managedOC) {
      let image = Image(entity: newImage, insertInto: managedOC)
      image.id = id
      image.title = title
      image.link = link
      image.cover = cover
      try managedOC.save()
    }
    throw PersistenceError.couldNotSaveObject
  }

  func deleteAllRecords() throws {
    guard let managedOC = CoreDataStore.managedObjectContext else {
      throw PersistenceError.managedObjectContextNotFound
    }
    let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Image")
    let deleteRequest = NSBatchDeleteRequest (fetchRequest: deleteFetch)
    do {
      try managedOC.execute(deleteRequest)
      try managedOC.save()
    } catch {

    }
  }
}
