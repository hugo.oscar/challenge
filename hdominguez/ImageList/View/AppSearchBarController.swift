//
//  AppSearchBarController.swift
//  hdominguez
//
//  Created by Hugo Oscar Dominguez Navarrete on 1/17/19.
//  Copyright © 2019 Hugo Oscar Dominguez Navarrete. All rights reserved.
//

import UIKit
import Material

class AppSearchBarController: SearchBarController {

  open override func prepare() {
    super.prepare()
    prepareStatusBar()
    prepareSearchBar()
  }
}

extension AppSearchBarController {

  fileprivate func prepareStatusBar() {
    statusBarStyle = .lightContent
  }

  fileprivate func prepareSearchBar() {

  }
}
