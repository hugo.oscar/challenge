//
//  ImageListView.swift
//  hdominguez
//
//  Created by Hugo Oscar Dominguez Navarrete on 1/15/19.
//  Copyright © 2019 Hugo Oscar Dominguez Navarrete. All rights reserved.
//

import UIKit
import Material
import SDWebImage
import Motion

class ImageListView: UIViewController {

  var searchBar: SearchBar?
  var tableView: TableView? = TableView()
  var presenter: ImageListPresenterProtocol?
  var imageList: [ImageModel] = []

  var dataSourceItems = [DataSourceItem]() {
    didSet {
      tableView?.reloadData()
    }
  }

  open override func viewDidLoad() {
    super.viewDidLoad()
    prepareSearchBar()
    prepareTableView()
    presenter?.viewDidLoad()
    tableView?.register(TableViewCell.self, forCellReuseIdentifier: "TableViewCell")
  }

  open override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
    self.navigationController?.setNavigationBarHidden(true, animated: false)
  }
}

extension ImageListView: ImageListViewProtocol {

  func showImages(with images: [ImageModel]) {
    imageList = images
    tableView?.reloadData()
  }

  func showError() {
    //print("Error")
  }

  func showLoading() {
    //print("Cargando")
  }

  func hideLoading() {
    //print("Terminando de Cargar")
  }
}

extension ImageListView {
  fileprivate func prepareSearchBar() {
    self.searchBar = searchBarController?.searchBar
    searchBarController?.searchBar.delegate = self
  }
  fileprivate func prepareTableView() {
    tableView?.prepare()
		guard let tableV = tableView as TableView? else {
			return
		}
    self.view.layout(tableV).edges()
    tableView?.delegate = self
    tableView?.dataSource = self
    tableView?.prefetchDataSource = self
		tableView?.keyboardDismissMode = .onDrag
  }
}

extension ImageListView: SearchBarDelegate {
  func searchBar(searchBar: SearchBar, didChange textField: UITextField, with text: String?) {
    if text != "" {
      tableView?.setContentOffset(CGPoint.zero, animated: true)
    }
    presenter?.uploadResultsWithString(searchString: text ?? "")
  }
}

extension ImageListView: TableViewDelegate, TableViewDataSource {

  func numberOfSections(in tableView: UITableView) -> Int {
      return 1
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath) as? TableViewCell else { return TableViewCell() }
    guard let image = imageList[indexPath.row] as ImageModel? else {
      return cell
    }

    cell.imageView?.image = nil
    cell.textLabel?.text = nil
    cell.detailTextLabel?.text = nil
		cell.textLabel?.motionIdentifier = image.title
    cell.imageView?.contentMode = UIView.ContentMode.scaleAspectFit
    cell.textLabel?.text = image.title
    cell.detailTextLabel?.text = image.link
    cell.imageView?.sd_setImage(
      with: URL(string: "https://i.imgur.com/\(image.cover).jpg"),
      placeholderImage: UIImage(named: "placeholder"),
      options: SDWebImageOptions.scaleDownLargeImages,
      completed: { (_, _, _, _) in
    })
		cell.imageView?.motionIdentifier = image.cover
		cell.transition(.fadeOut, .scale(0.75))
    cell.dividerColor = Color.grey.lighten2
    return cell
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return imageList.count
  }

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		guard let image = imageList[indexPath.row] as ImageModel? else {
			return
		}
		presenter?.showImageDetail(forImage: image)
  }

  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 80
  }
}

extension ImageListView: UITableViewDataSourcePrefetching {
  func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
    if indexPaths.last?.row == (imageList.count - 1) {
      presenter?.updateListWithNextIteration(searchString: self.searchBar?.textField.text ?? "")
    }
  }
}

extension TableViewCell {
  override open func layoutSubviews() {
    super.layoutSubviews()
    self.imageView?.frame.size = CGSize(width: 80, height: 80)
    self.textLabel?.frame.origin = CGPoint(x: 100, y: self.textLabel?.frame.origin.y ?? 0)
    self.detailTextLabel?.frame.origin = CGPoint(x: 100, y: self.detailTextLabel?.frame.origin.y ?? 0)
  }
}
