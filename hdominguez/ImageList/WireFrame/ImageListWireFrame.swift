//
//  ImageListWireFrame.swift
//  hdominguez
//
//  Created by Hugo Oscar Dominguez Navarrete on 1/15/19.
//  Copyright © 2019 Hugo Oscar Dominguez Navarrete. All rights reserved.
//

import UIKit
import Material

class ImageListWireFrame: ImageListWireFrameProtocol {
  class func createImageListModule() -> UIViewController {
    let navController = mainStoryboard.instantiateViewController(withIdentifier: "ImageListNavigationController")
    if let view = navController.children.first as? ImageListView {

      let presenter: ImageListPresenterProtocol & ImageListInteractorOutputProtocol = ImageListPresenter()
      let interactor: ImageListInteractorInputProtocol & ImageListRemoteDataManagerOutputProtocol = ImageListInteractor()
      let localDataManager: ImageListLocalDataManagerInputProtocol = ImageListLocalDataManager()
      let remoteDataManager: ImageListRemoteDataManagerInputProtocol = ImageListRemoteDataManager()
      let wireFrame: ImageListWireFrameProtocol = ImageListWireFrame()

      view.presenter = presenter
      presenter.view = view
      presenter.wireFrame = wireFrame
      presenter.interactor = interactor
      interactor.presenter = presenter
      interactor.localDatamanager = localDataManager
      interactor.remoteDatamanager = remoteDataManager
      remoteDataManager.remoteRequestHandler = interactor

      return navController
    }
    return UIViewController()
  }

  static var mainStoryboard: UIStoryboard {
    return UIStoryboard(name: "Main", bundle: Bundle.main)
  }

	func presentImageDetailScreen(from view: ImageListViewProtocol, forImage image: ImageModel) {
    let imageDetailViewController = ImageDetailWireFrame.createImageDetailModule(forImage: image)
		if let sourceView = view as? UIViewController {
			sourceView.present(imageDetailViewController, animated: true, completion: {
			})
		}
	}
}
