//
//  ImageListPresenter.swift
//  hdominguez
//
//  Created by Hugo Oscar Dominguez Navarrete on 1/15/19.
//  Copyright © 2019 Hugo Oscar Dominguez Navarrete. All rights reserved.
//

class ImageListPresenter: ImageListPresenterProtocol {

  weak var view: ImageListViewProtocol?
  var interactor: ImageListInteractorInputProtocol?
  var wireFrame: ImageListWireFrameProtocol?

  func viewDidLoad() {

  }

  func uploadResultsWithString(searchString: String) {
    view?.showLoading()
    self.interactor?.retrieveImageListWithString(searchString: searchString)
  }

  func updateListWithNextIteration(searchString: String) {
    view?.showLoading()
    self.interactor?.retrieveImageListWithIteration(searchString: searchString)
  }

  func showImageDetail(forImage image: ImageModel) {
		guard let tempView = self.view as ImageListViewProtocol? else { return }
    wireFrame?.presentImageDetailScreen(from: tempView, forImage: image)
  }

}

extension ImageListPresenter: ImageListInteractorOutputProtocol {

  func didRetrieveImages(_ images: [ImageModel]) {
    view?.hideLoading()
    view?.showImages(with: images)
  }

  func onError() {
    view?.hideLoading()
    view?.showError()
  }
}
