//
//  ImageListProtocols.swift
//  hdominguez
//
//  Created by Hugo Oscar Dominguez Navarrete on 1/15/19.
//  Copyright © 2019 Hugo Oscar Dominguez Navarrete. All rights reserved.
//

import UIKit

protocol ImageListViewProtocol: class {
  var presenter: ImageListPresenterProtocol? { get set }

  // PRESENTER -> VIEW
  func showImages(with images: [ImageModel])

  func showError()

  func showLoading()

  func hideLoading()
}

protocol ImageListWireFrameProtocol: class {
  static func createImageListModule() -> UIViewController
	func presentImageDetailScreen(from view: ImageListViewProtocol, forImage image: ImageModel)
}

protocol ImageListPresenterProtocol: class {
  var view: ImageListViewProtocol? { get set }
  var interactor: ImageListInteractorInputProtocol? { get set }
  var wireFrame: ImageListWireFrameProtocol? { get set }
  func viewDidLoad()
  func uploadResultsWithString(searchString: String)
  func updateListWithNextIteration(searchString: String)
	func showImageDetail(forImage image: ImageModel)
}

protocol ImageListInteractorOutputProtocol: class {
  func didRetrieveImages(_ images: [ImageModel])
  func onError()
}

protocol ImageListInteractorInputProtocol: class {
  var presenter: ImageListInteractorOutputProtocol? { get set }
  var localDatamanager: ImageListLocalDataManagerInputProtocol? { get set }
  var remoteDatamanager: ImageListRemoteDataManagerInputProtocol? { get set }

  func retrieveImageList()
  func retrieveImageListWithString(searchString: String)
  func retrieveImageListWithIteration(searchString: String)
}

protocol ImageListDataManagerInputProtocol: class {

}

protocol ImageListRemoteDataManagerInputProtocol: class {
  var remoteRequestHandler: ImageListRemoteDataManagerOutputProtocol? { get set }

  func retrieveImageList()
  func retrieveImageListWithString(string: String, iteration: Int)
}

protocol ImageListRemoteDataManagerOutputProtocol: class {
  func onImageRetrieved(_ images: [ImageModel])
  func onError()
}

protocol ImageListLocalDataManagerInputProtocol: class {
  func retrieveImageList() throws -> [Image]
  func saveImage(id: String, title: String, link: String, cover: String) throws
  func deleteAllRecords() throws
}
