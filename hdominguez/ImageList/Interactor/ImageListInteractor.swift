//
//  ImageListInteractor.swift
//  hdominguez
//
//  Created by Hugo Oscar Dominguez Navarrete on 1/15/19.
//  Copyright © 2019 Hugo Oscar Dominguez Navarrete. All rights reserved.
//
import UIKit

class ImageListInteractor: ImageListInteractorInputProtocol {
  weak var presenter: ImageListInteractorOutputProtocol?
  var localDatamanager: ImageListLocalDataManagerInputProtocol?
  var remoteDatamanager: ImageListRemoteDataManagerInputProtocol?

  var searchTimer: Timer?
  var counter: Int = 1

  func retrieveImageList() {
    do {
      if let imageList = try localDatamanager?.retrieveImageList() {
				let imageModelList = imageList.map {
					return ImageModel(id: $0.id ?? "", title: $0.title ?? "", link: $0.link ?? "", cover: $0.cover ?? "")
        }
        if  imageModelList.isEmpty {
          remoteDatamanager?.retrieveImageList()
        } else {
          presenter?.didRetrieveImages(imageModelList)
        }
      } else {
        remoteDatamanager?.retrieveImageList()
      }
    } catch {
      presenter?.didRetrieveImages([])
    }
  }

  func retrieveImageListWithIteration(searchString: String) {
    self.remoteDatamanager?.retrieveImageListWithString(string: searchString, iteration: counter + 1)
    counter = counter + 1
  }

  func retrieveImageListWithString(searchString: String) {
    if !searchString.isEmpty {
      if searchTimer != nil {
        searchTimer?.invalidate()
        searchTimer = nil
      }
      searchTimer = Timer.scheduledTimer(withTimeInterval: 0.25, repeats: false, block: { [unowned self] _ in
        do {
          try self.localDatamanager?.deleteAllRecords()
          self.counter = 1
        } catch {
          self.presenter?.onError()
        }
        self.remoteDatamanager?.retrieveImageListWithString(string: searchString, iteration: 1)
      })
    }
  }
}

extension ImageListInteractor: ImageListRemoteDataManagerOutputProtocol {

  func onImageRetrieved(_ images: [ImageModel]) {
    for imageModel in images {
      do {
        try localDatamanager?.saveImage(id: imageModel.id, title: imageModel.title, link: imageModel.link, cover: imageModel.cover)
      } catch {
			}
            self.retrieveImageList()
          }
        }
  func onError() {
    presenter?.onError()
  }
}
