//
//  EndPoints.swift
//  hdominguez
//
//  Created by Hugo Oscar Dominguez Navarrete on 1/15/19.
//  Copyright © 2019 Hugo Oscar Dominguez Navarrete. All rights reserved.
//

import Foundation

struct API {
  static let baseUrl = "https://api.imgur.com/3/gallery/search/time/"
}

protocol Endpoint {
  var path: String { get }
  var url: String { get }
}

enum Endpoints {

  enum Images: Endpoint {
    case fetch

    public var path: String {
      switch self {
      case .fetch:
				return "1?q=cats"
      }
    }
    public var url: String {
      switch self {
      case .fetch:
				return "\(API.baseUrl)\(path)"
      }
    }
    public func searchWithPage(page: Int, searchValue: String) -> String {
      switch self {
      case .fetch:
				return "\(API.baseUrl)\(page.description)?q=\(searchValue)"
      }
    }
  }
}
