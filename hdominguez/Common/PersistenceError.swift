//
//  PersistenceError.swift
//  hdominguez
//
//  Created by Hugo Oscar Dominguez Navarrete on 1/15/19.
//  Copyright © 2019 Hugo Oscar Dominguez Navarrete. All rights reserved.
//

import Foundation

enum PersistenceError: Error {
  case managedObjectContextNotFound
  case couldNotSaveObject
  case objectNotFound
}
